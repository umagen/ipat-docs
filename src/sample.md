# 呼び出し側サンプルソース

## 目次

* [Excel2016(VBA) / VisualBasic6.0 【dataモード】](#id-section1)
* [Excel2016(VBA) / VisualBasic6.0 【fileモード】](#id-section2)
* [Excel2016(VBA) / VisualBasic6.0 【statモード】](#id-section3)
* [Excel2016(VBA) / VisualBasic6.0 【depositモード】](#id-section4)
* [Excel2016(VBA) / VisualBasic6.0 【withdrawモード】](#id-section5)
* [VisualBasic2015 【dataモード】](#id-section6)
* [VisualBasic2015 【fileモード】](#id-section7)
* [VisualBasic2015 【statモード】](#id-section8)
* [Python 【dataモード】](#id-section9)
* <a href="https://note.com/777hiyoko7777/n/n84d150b2cc64" target="blank">Python 【fileモード】</a> ひよこさんのnoteへのリンクです
* [Windows/macOSX/Linuxでコマンドプロントからの【dataモード】](#id-section10)

※ ここに無いモードやオプションのサンプルは[簡単投票ソフト（検証用）](https://ipat-docs.readthedocs.io/ja/latest/demo.html)で公開しているソース(VisualBasic2015)を参考にしてください。

<div id='id-section1'/>

## Excel2016(VBA) / VisualBasic6.0 【dataモード】
* ツール / プロジェクト → 参照設定 → `Windows Script Host Object Model` の指定が必要です

```vbnet
'========================================================================================================
'
'   IPATGO サンプル Microsoft Excel 2016(VBA)
'
'   dataモードによる投票を実行
'
'========================================================================================================

' IPATログイン情報構造体
Type IpatLogin
     InetID As String 'INET_ID
     UserNo As String '加入者番号
     PassNo As String '暗証番号
     ParsNo As String 'P-ARS番号
End Type

Sub main()
Dim iplg As IpatLogin
Dim vdata As String '投票データ1件

' IPATログイン情報を指定
    iplg.InetID = "XXXXXXXX" ' <-----実際のINET-IDを設定します
    iplg.UserNo = "99999999" ' <-----実際の加入者番号を設定します
    iplg.PassNo = "9999"     ' <-----実際の暗証番号を設定します
    iplg.ParsNo = "9999"     ' <-----実際のP-ARS番号を設定します
 
' 投票データを1件作成
    vdata = "20190526,TOKYO,11,FUKUSYO,NORMAL,,06,100"    ' <-----実際の投票データを設定します
    
' dataモードで投票実行
    If Vote_data("c:\umagen\ipatgo\", iplg, vdata) = 0 Then
        MsgBox ("投票処理を実行しました")    ' <-----自動運転ではこのコマンドは省略します
    Else
        MsgBox ("投票処理が失敗しました")    ' <-----自動運転ではこのコマンドは省略します
    End If
    
End Sub

'========================================================================================================
'
'   機能    指定されたIPATIDでdataモードによる投票を実行
'
'   引数    filepath：ipatgo.exeがあるパスを指定
'           IL      ：IPATログイン情報を指定
'           vd      ：投票データ
'
'   戻り値  0　　　 ：投票処理正常実行
'           0以外   ：投票処理失敗
'
'========================================================================================================
Function Vote_data(ByVal filepath As String, ByRef IL As IpatLogin, ByVal vd As String) As Long
Dim obj As New IWshRuntimeLibrary.WshShell

    If obj.Run(filepath & "ipatgo.exe" & " " & _
                    "data" & " " & _
                    IL.InetID & " " & _
                    IL.UserNo & " " & _
                    IL.PassNo & " " & _
                    IL.ParsNo & " " & _
                    vd, _
                    0, True) <> 0 Then
        Vote_data = -1   '投票処理失敗
        Exit Function
    End If

    Vote_data = 0   '投票処理正常実行

End Function

```
<div id='id-section2'/>

## Excel2016(VBA) / VisualBasic6.0 【fileモード】
* ツール / プロジェクト → 参照設定 → `Windows Script Host Object Model` の指定が必要です

```vbnet
'========================================================================================================
'
'   IPATGO サンプル Microsoft Excel 2016(VBA)
'
'   fileモードによる投票を実行
'
'========================================================================================================

' IPATログイン情報構造体
Type IpatLogin
     InetID As String 'INET_ID
     UserNo As String '加入者番号
     PassNo As String '暗証番号
     ParsNo As String 'P-ARS番号
End Type

Sub main()
Dim iplg As IpatLogin
Dim vdata1 As String '投票データ1行目
Dim vdata2 As String '投票データ2行目

' IPATログイン情報を指定
    iplg.InetID = "XXXXXXXX" ' <-----実際のINET-IDを設定します
    iplg.UserNo = "99999999" ' <-----実際の加入者番号を設定します
    iplg.PassNo = "9999"     ' <-----実際の暗証番号を設定します
    iplg.ParsNo = "9999"     ' <-----実際のP-ARS番号を設定します
 
' 投票データを2件作成
    vdata1 = "20190526,TOKYO,11,WIDE,NORMAL,,07-13,100"    ' <-----実際の情報を設定します
    vdata2 = "20190526,KYOTO,12,TANSYO,NORMAL,,12,100"     ' <-----実際の情報を設定します
    
' 投票ファイルを任意の場所に作成
    Open "c:\umagen\ipatgo\test.txt" For Output As #1
       Print #1, vdata1
       Print #1, vdata2
    Close #1
    
' fileモードで投票実行
    If Vote_file("c:\umagen\ipatgo\", iplg, "c:\umagen\ipatgo\test.txt") = 0 Then
        MsgBox ("投票処理を実行しました")    ' <-----自動運転ではこのコマンドは省略します
    Else
        MsgBox ("投票処理が失敗しました")    ' <-----自動運転ではこのコマンドは省略します
    End If
    
End Sub
'========================================================================================================
'
'   機能    指定されたIPATIDでfileモードによる投票を実行
'
'   引数    filepath：ipatgo.exeがあるパスを指定
'           IL      ：IPATログイン情報を指定
'           vf      ：投票ファイルのフルパスを指定
'
'   戻り値  0　　　 ：投票処理正常実行
'           0以外   ：投票処理失敗
'
'========================================================================================================
Function Vote_file(ByVal filepath As String, ByRef IL As IpatLogin, ByVal vf As String) As Long
Dim obj As New IWshRuntimeLibrary.WshShell

    If obj.Run(filepath & "ipatgo.exe" & " " & _
                    "file" & " " & _
                    IL.InetID & " " & _
                    IL.UserNo & " " & _
                    IL.PassNo & " " & _
                    IL.ParsNo & " " & _
                    vf, _
                    0, True) <> 0 Then
        Vote_file = -1   '投票処理失敗
        Exit Function
    End If

    Vote_file = 0   '投票処理正常実行

End Function

```
<div id='id-section3'/>

## Excel2016(VBA) / VisualBasic6.0 【statモード】
* ツール / プロジェクト → 参照設定 → `Windows Script Host Object Model` の指定が必要です

```vbnet
'========================================================================================================
'
'   IPATGO サンプル Microsoft Excel 2016(VBA)
'
'   IPAT購入状況を照会する
'
'========================================================================================================
' INIファイル文字列情報取得関数API定義
Public Declare Function GetPrivateProfileString Lib "kernel32" _
                         Alias "GetPrivateProfileStringA" _
                         (ByVal lpApplicationName As String, _
                          ByVal lpKeyName As Any, _
                          ByVal lpDefault As String, _
                          ByVal lpReturnedString As String, _
                          ByVal nSize As Long, _
                          ByVal lpFileName As String) As Long

' IPATログイン情報構造体
Type IpatLogin
    InetID As String 'INET_ID
    UserNo As String '加入者番号
    PassNo As String '暗証番号
    ParsNo As String 'P-ARS番号
End Type

' IPAT購入状況情報構造体
Type StatInfo
    date As String                ' 取得年月日
    time As String                ' 取得時刻
    total_vote_amount As String   ' 累計購入金額
    total_repayment As String     ' 累計払戻金額
    daily_vote_amount As String   ' 1日分購入金額
    daily_repayment As String     ' 1日分払戻金額
    limit_vote_amount As String   ' 購入限度額
    limit_vote_count As String    ' 購入可能件数
End Type

Sub main()
Dim iplg As IpatLogin
Dim stin As StatInfo


' IPATログイン情報を指定
    iplg.InetID = "XXXXXXXX" ' <-----実際のINET-IDを設定します
    iplg.UserNo = "99999999" ' <-----実際の加入者番号を設定します
    iplg.PassNo = "9999"     ' <-----実際の暗証番号を設定します
    iplg.ParsNo = "9999"     ' <-----実際のP-ARS番号を設定します
    
    If Get_Stat("c:\umagen\ipatgo\", iplg, stin) = 0 Then
       
       MsgBox ("取得年月日：" & stin.date & vbCrLf & _
               "取得時刻：" & stin.time & vbCrLf & _
               "累計購入金額：" & stin.total_vote_amount & vbCrLf & _
               "累計払戻金額：" & stin.total_repayment & vbCrLf & _
               "1日分購入金額：" & stin.daily_vote_amount & vbCrLf & _
               "1日分払戻金額：" & stin.daily_repayment & vbCrLf & _
               "購入限度額：" & stin.limit_vote_amount & vbCrLf & _
               "購入可能件数：" & stin.limit_vote_count)
    Else
  
       MsgBox ("購入状況照会に失敗しました")

    End If
      
End Sub

'========================================================================================================
'
'   機能    指定されたIPATIDの購入状況を返す
'
'   引数    filepath：ipatgo.exeがあるパスを指定
'           IL      ：IPATログイン情報を指定
'           ST      ：購入状況情報の格納用
'
'   戻り値  0　　　 ：取得成功
'           0以外   ：取得失敗
'
'========================================================================================================
Function Get_Stat(ByVal filepath As String, ByRef IL As IpatLogin, ByRef ST As StatInfo) As Long
Dim obj As New IWshRuntimeLibrary.WshShell
Dim value As String * 256

' statモードで取得実行
    If obj.Run(filepath & "ipatgo.exe" & " " & _
                    "stat" & " " & _
                    IL.InetID & " " & _
                    IL.UserNo & " " & _
                    IL.PassNo & " " & _
                    IL.ParsNo, _
                    0, True) <> 0 Then
        Get_Stat = -1   '取得失敗
        Exit Function
    End If

' 更新されたstat.iniから値を取得
    Call GetPrivateProfileString("stat", "date", vbNullString, value, Len(value), (filepath & "stat.ini"))
    ST.date = Left$(value, InStr(1, value, vbNullChar) - 1)
    Call GetPrivateProfileString("stat", "time", vbNullString, value, Len(value), (filepath & "stat.ini"))
    ST.time = Left$(value, InStr(1, value, vbNullChar) - 1)
    Call GetPrivateProfileString("stat", "total_vote_amount", vbNullString, value, Len(value), (filepath & "stat.ini"))
    ST.total_vote_amount = Left$(value, InStr(1, value, vbNullChar) - 1)
    Call GetPrivateProfileString("stat", "total_repayment", vbNullString, value, Len(value), (filepath & "stat.ini"))
    ST.total_repayment = Left$(value, InStr(1, value, vbNullChar) - 1)
    Call GetPrivateProfileString("stat", "daily_vote_amount", vbNullString, value, Len(value), (filepath & "stat.ini"))
    ST.daily_vote_amount = Left$(value, InStr(1, value, vbNullChar) - 1)
    Call GetPrivateProfileString("stat", "daily_repayment", vbNullString, value, Len(value), (filepath & "stat.ini"))
    ST.daily_repayment = Left$(value, InStr(1, value, vbNullChar) - 1)
    Call GetPrivateProfileString("stat", "limit_vote_amount", vbNullString, value, Len(value), (filepath & "stat.ini"))
    ST.limit_vote_amount = Left$(value, InStr(1, value, vbNullChar) - 1)
    Call GetPrivateProfileString("stat", "limit_vote_count", vbNullString, value, Len(value), (filepath & "stat.ini"))
    ST.limit_vote_count = Left$(value, InStr(1, value, vbNullChar) - 1)
    
    Get_Stat = 0   '取得成功

End Function


```
<div id='id-section4'/>

## Excel2016(VBA) / VisualBasic6.0 【depositモード】
* ツール / プロジェクト → 参照設定 → `Windows Script Host Object Model` の指定が必要です

```vbnet
'========================================================================================================
'
'   IPATGO サンプル Microsoft Excel 2016(VBA)
'
'   depositモードによる入金を実行
'
'========================================================================================================

' IPATログイン情報構造体
Type IpatLogin
     InetID As String 'INET_ID
     UserNo As String '加入者番号
     PassNo As String '暗証番号
     ParsNo As String 'P-ARS番号
End Type

Sub main()
Dim iplg As IpatLogin
Dim money As Double '入金額

' IPATログイン情報を指定
    iplg.InetID = "XXXXXXXX" ' <-----実際のINET-IDを設定します
    iplg.UserNo = "99999999" ' <-----実際の加入者番号を設定します
    iplg.PassNo = "9999"     ' <-----実際の暗証番号を設定します
    iplg.ParsNo = "9999"     ' <-----実際のP-ARS番号を設定します
 
' 入金額をセット
    money = 100   ' <-----実際の入金額を設定します（100～999999900）
    
' depositモードによる入金を実行
    If fnc_deposit("c:\umagen\ipatgo\", iplg, money) = 0 Then
        MsgBox ("入金処理を実行しました")
    Else
        MsgBox ("入金処理が失敗しました")
    End If
    
End Sub

'========================================================================================================
'
'   機能    指定されたIPATIDでdepositモードによる入金を実行
'
'   引数    filepath：ipatgo.exeがあるパスを指定
'           IL      ：IPATログイン情報を指定
'           mn      ：入金額（100～999999900）
'
'   戻り値  0　　　 ：入金処理正常実行
'           0以外   ：入金処理失敗
'
'========================================================================================================
Function fnc_deposit(ByVal filepath As String, ByRef IL As IpatLogin, ByVal mn As Double) As Long
Dim obj As New IWshRuntimeLibrary.WshShell

    If obj.Run(filepath & "ipatgo.exe" & " " & _
                    "deposit" & " " & _
                    IL.InetID & " " & _
                    IL.UserNo & " " & _
                    IL.PassNo & " " & _
                    IL.ParsNo & " " & _
                    mn, _
                    0, True) <> 0 Then
        fnc_deposit = -1   '入金処理失敗
        Exit Function
    End If

    fnc_deposit = 0   '入金処理正常実行

End Function

```
<div id='id-section5'/>

## Excel2016(VBA) / VisualBasic6.0 【withdrawモード】
* ツール / プロジェクト → 参照設定 → `Windows Script Host Object Model` の指定が必要です

```vbnet
'========================================================================================================
'
'   IPATGO サンプル Microsoft Excel 2016(VBA)
'
'   withdrawモードによる出金を実行
'
'========================================================================================================

' IPATログイン情報構造体
Type IpatLogin
     InetID As String 'INET_ID
     UserNo As String '加入者番号
     PassNo As String '暗証番号
     ParsNo As String 'P-ARS番号
End Type

Sub main()
Dim iplg As IpatLogin

' IPATログイン情報を指定
    iplg.InetID = "XXXXXXXX" ' <-----実際のINET-IDを設定します
    iplg.UserNo = "99999999" ' <-----実際の加入者番号を設定します
    iplg.PassNo = "9999"     ' <-----実際の暗証番号を設定します
    iplg.ParsNo = "9999"     ' <-----実際のP-ARS番号を設定します
    
' withdrawモードによる全額出金を実行
    If fnc_withdraw("c:\umagen\ipatgo\", iplg) = 0 Then
        MsgBox ("出金処理を実行しました")
    Else
        MsgBox ("出金処理が失敗しました")
    End If
    
End Sub

'========================================================================================================
'
'   機能    指定されたIPATIDでwithdrawモードによる全額出金を実行
'
'   引数    filepath：ipatgo.exeがあるパスを指定
'           IL      ：IPATログイン情報を指定
'
'   戻り値  0　　　 ：出金処理正常実行
'           0以外   ：出金処理失敗
'
'========================================================================================================
Function fnc_withdraw(ByVal filepath As String, ByRef IL As IpatLogin) As Long
Dim obj As New IWshRuntimeLibrary.WshShell

    If obj.Run(filepath & "ipatgo.exe" & " " & _
                    "withdraw" & " " & _
                    IL.InetID & " " & _
                    IL.UserNo & " " & _
                    IL.PassNo & " " & _
                    IL.ParsNo & " " & _
                    0, True) <> 0 Then
        fnc_withdraw = -1   '出金処理失敗
        Exit Function
    End If

    fnc_withdraw = 0   '出金処理正常実行

End Function
```

<div id='id-section6'/>

## VisualBasic2015 【dataモード】

```vbnet
Public Class Form1
    ' IPATログイン情報構造体
    Public Structure IpatLogin
        Public InetID As String 'INET_ID
        Public UserNo As String '加入者番号
        Public PassNo As String '暗証番号
        Public ParsNo As String 'P-ARS番号
    End Structure
    '========================================================================================================
    '
    '   IPATGO サンプル Microsoft Visual Basic 2015
    '
    '   dataモードによる投票を実行
    '
    '========================================================================================================
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim iplg As IpatLogin
        Dim vdata As String '投票データ1件

        ' IPATログイン情報を指定
        iplg.InetID = "XXXXXXXX" ' <-----実際のINET-IDを設定します
        iplg.UserNo = "99999999" ' <-----実際の加入者番号を設定します
        iplg.PassNo = "9999"     ' <-----実際の暗証番号を設定します
        iplg.ParsNo = "9999"     ' <-----実際のP-ARS番号を設定します

        ' 投票データを1件作成
        vdata = "20190526,TOKYO,11,FUKUSYO,NORMAL,,06,100"    ' <-----実際の情報を設定します"

        ' dataモードで投票実行
        If Vote_data("c:\umagen\ipatgo\", iplg, vdata) = 0 Then
            MsgBox("投票処理を実行しました")    ' <-----自動運転ではこのコマンドは省略します
        Else
            MsgBox("投票処理が失敗しました")    ' <-----自動運転ではこのコマンドは省略します
        End If

    End Sub
    '========================================================================================================
    '
    '   機能    指定されたIPATIDでdataモードによる投票を実行
    '
    '   引数    filepath：ipatgo.exeがあるパスを指定
    '           IL      ：IPATログイン情報を指定
    '           vd      ：投票データ
    '
    '   戻り値  0　　　 ：投票処理正常実行
    '           0以外   ：投票処理失敗
    '
    '========================================================================================================
    Public Function Vote_data(ByVal filepath As String, ByVal IL As IpatLogin, ByVal vd As String) As Long

        Dim psInfo As New ProcessStartInfo()

        psInfo.FileName = filepath & "ipatgo.exe" ' 実行するファイル
        psInfo.CreateNoWindow = True ' コンソール・ウィンドウを開かない
        psInfo.UseShellExecute = False ' シェル機能を使用しない
        psInfo.Arguments = "data" & " " &
                    IL.InetID & " " &
                    IL.UserNo & " " &
                    IL.PassNo & " " &
                    IL.ParsNo & " " &
                    vd

        Dim p As System.Diagnostics.Process = System.Diagnostics.Process.Start(psInfo)

        p.WaitForExit() ' 終了するまで待つ

        Vote_data = p.ExitCode

    End Function

End Class
```
<div id='id-section7'/>

## VisualBasic2015 【fileモード】

```vbnet
Public Class Form1
    ' IPATログイン情報構造体
    Public Structure IpatLogin
        Public InetID As String 'INET_ID
        Public UserNo As String '加入者番号
        Public PassNo As String '暗証番号
        Public ParsNo As String 'P-ARS番号
    End Structure

    '========================================================================================================
    '
    '   IPATGO サンプル Microsoft Visual Basic 2015
    '
    '   fileモードによる投票を実行
    '
    '========================================================================================================
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim iplg As IpatLogin
        Dim vdata1 As String '投票データ1行目
        Dim vdata2 As String '投票データ2行目

        ' IPATログイン情報を指定
        iplg.InetID = "XXXXXXXX" ' <-----実際のINET-IDを設定します
        iplg.UserNo = "99999999" ' <-----実際の加入者番号を設定します
        iplg.PassNo = "9999"     ' <-----実際の暗証番号を設定します
        iplg.ParsNo = "9999"     ' <-----実際のP-ARS番号を設定します

        ' 投票データを2件作成
        vdata1 = "20190526,TOKYO,11,WIDE,NORMAL,,07-13,100"    ' <-----実際の情報を設定します
        vdata2 = "20190526,KYOTO,12,TANSYO,NORMAL,,12,100"     ' <-----実際の情報を設定します

        ' 投票ファイルを任意の場所に作成
        Dim textFile As System.IO.StreamWriter
        textFile = New System.IO.StreamWriter("c:\umagen\ipatgo\test.txt", False, System.Text.Encoding.Default)
        textFile.WriteLine(vdata1)
        textFile.WriteLine(vdata2)
        textFile.Close()

        ' fileモードで投票実行
        If Vote_file("c:\umagen\ipatgo\", iplg, "c:\umagen\ipatgo\test.txt") = 0 Then
            MsgBox("投票処理を実行しました")    ' <-----自動運転ではこのコマンドは省略します
        Else
            MsgBox("投票処理が失敗しました")    ' <-----自動運転ではこのコマンドは省略します
        End If

    End Sub

    '========================================================================================================
    '
    '   機能    指定されたIPATIDでfileモードによる投票を実行
    '
    '   引数    filepath：ipatgo.exeがあるパスを指定
    '           IL      ：IPATログイン情報を指定
    '           vf      ：投票ファイルのフルパスを指定
    '
    '   戻り値  0　　　 ：投票処理正常実行
    '           0以外   ：投票処理失敗
    '
    '========================================================================================================
    Public Function Vote_file(ByVal filepath As String, ByVal IL As IpatLogin, ByVal vf As String) As Long
        Dim psInfo As New ProcessStartInfo()

        psInfo.FileName = filepath & "ipatgo.exe" ' 実行するファイル
        psInfo.CreateNoWindow = True ' コンソール・ウィンドウを開かない
        psInfo.UseShellExecute = False ' シェル機能を使用しない
        psInfo.Arguments = "file" & " " &
                    IL.InetID & " " &
                    IL.UserNo & " " &
                    IL.PassNo & " " &
                    IL.ParsNo & " " &
                    vf

        Dim p As System.Diagnostics.Process = System.Diagnostics.Process.Start(psInfo)

        p.WaitForExit() ' 終了するまで待つ

        Vote_file = p.ExitCode

    End Function

End Class
```

<div id='id-section8'/>

## VisualBasic2015 【statモード】

```vbnet
Public Class Form1

    ' INIファイル値呼び出しAPI
    Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (
        ByVal lpApplicationName As String,
        ByVal lpKeyName As String,
        ByVal lpDefault As String,
        ByVal lpReturnedString As String,
        ByVal nSize As Integer,
        ByVal lpFileName As String) As Integer

    ' IPATログイン情報構造体
    Public Structure IpatLogin
        Public InetID As String 'INET_ID
        Public UserNo As String '加入者番号
        Public PassNo As String '暗証番号
        Public ParsNo As String 'P-ARS番号
    End Structure

    ' IPAT購入状況情報構造体
    Public Structure StatInfo
        Public stat_date As String          ' 取得年月日
        Public stat_time As String          ' 取得時刻
        Public total_vote_amount As String  ' 累計購入金額
        Public total_repayment As String    ' 累計払戻金額
        Public daily_vote_amount As String  ' 1日分購入金額
        Public daily_repayment As String    ' 1日分払戻金額
        Public limit_vote_amount As String  ' 購入限度額
        Public limit_vote_count As String   ' 購入可能件数
    End Structure

    '========================================================================================================
    '
    '   IPATGO サンプル 
    '
    '   IPAT購入状況を照会する
    '
    '========================================================================================================
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim iplg As IpatLogin
        Dim stin As StatInfo

        stin = New StatInfo()

        ' IPATログイン情報を指定
        iplg.InetID = "XXXXXXXX" ' <-----実際のINET-IDを設定します
        iplg.UserNo = "99999999" ' <-----実際の加入者番号を設定します
        iplg.PassNo = "9999"     ' <-----実際の暗証番号を設定します
        iplg.ParsNo = "9999"     ' <-----実際のP-ARS番号を設定します

        ' statモードで取得実行
        If Get_Stat("c:\umagen\ipatgo\", iplg, stin) = 0 Then

            MsgBox("取得年月日：" & stin.stat_date & vbCrLf &
                   "取得時刻：" & stin.stat_time & vbCrLf &
                   "累計購入金額：" & stin.total_vote_amount & vbCrLf &
                   "累計払戻金額：" & stin.total_repayment & vbCrLf &
                   "1日分購入金額：" & stin.daily_vote_amount & vbCrLf &
                   "1日分払戻金額：" & stin.daily_repayment & vbCrLf &
                   "購入限度額：" & stin.limit_vote_amount & vbCrLf &
                   "購入可能件数：" & stin.limit_vote_count)
        Else
            MsgBox("購入状況照会に失敗しました")
        End If

    End Sub
    '========================================================================================================
    '
    '   機能    指定されたIPATIDの購入状況を返す
    '
    '   引数    filepath：ipatgo.exeがあるパスを指定
    '           IL      ：IPATログイン情報を指定
    '           stin    ：購入状況情報の格納用
    '
    '   戻り値  0　　　 ：取得成功
    '           0以外   ：取得失敗
    '
    '========================================================================================================
    Function Get_Stat(ByVal filepath As String, ByVal IL As IpatLogin, ByRef ST As StatInfo) As Long
        Dim ret As Integer
        Const def As String = vbNullString
        Dim buff As String = New String(" ", 1024)

        Dim psInfo As New ProcessStartInfo()

        psInfo.FileName = filepath & "ipatgo.exe" ' 実行するファイル
        psInfo.CreateNoWindow = True ' コンソール・ウィンドウを開かない
        psInfo.UseShellExecute = False ' シェル機能を使用しない
        psInfo.Arguments = "stat" & " " &
                    IL.InetID & " " &
                    IL.UserNo & " " &
                    IL.PassNo & " " &
                    IL.ParsNo

        Dim p As System.Diagnostics.Process = System.Diagnostics.Process.Start(psInfo)

        p.WaitForExit() ' 終了するまで待つ

        If p.ExitCode <> 0 Then
            Get_Stat = p.ExitCode
            Exit Function
        End If

        ' 更新されたstat.iniから値を取得
        Dim iniFile As String = filepath & "\stat.ini"

        ret = GetPrivateProfileString("stat", "date", def, buff, buff.Length, (iniFile))
        ST.stat_date = buff.Substring(0, buff.IndexOf(vbNullChar))
        ret = GetPrivateProfileString("stat", "time", def, buff, buff.Length, (iniFile))
        ST.stat_time = buff.Substring(0, buff.IndexOf(vbNullChar))
        ret = GetPrivateProfileString("stat", "total_vote_amount", def, buff, buff.Length, (iniFile))
        ST.total_vote_amount = buff.Substring(0, buff.IndexOf(vbNullChar))
        ret = GetPrivateProfileString("stat", "total_repayment", def, buff, buff.Length, (iniFile))
        ST.total_repayment = buff.Substring(0, buff.IndexOf(vbNullChar))
        ret = GetPrivateProfileString("stat", "daily_vote_amount", def, buff, buff.Length, (iniFile))
        ST.daily_vote_amount = buff.Substring(0, buff.IndexOf(vbNullChar))
        ret = GetPrivateProfileString("stat", "daily_repayment", def, buff, buff.Length, (iniFile))
        ST.daily_repayment = buff.Substring(0, buff.IndexOf(vbNullChar))
        ret = GetPrivateProfileString("stat", "limit_vote_amount", def, buff, buff.Length, (iniFile))
        ST.limit_vote_amount = buff.Substring(0, buff.IndexOf(vbNullChar))
        ret = GetPrivateProfileString("stat", "limit_vote_count", def, buff, buff.Length, (iniFile))
        ST.limit_vote_count = buff.Substring(0, buff.IndexOf(vbNullChar))

        Get_Stat = 0   '取得成功

    End Function

End Class
```

<div id='id-section9'/>

## Python 【dataモード】

```python
import subprocess

inetid      = "XXXXXXXX"
user_number = "99999999"
password    = "9999"
pars_number = "9999"

vdata = "20190526,TOKYO,11,FUKUSYO,NORMAL,,06,100"

subprocess.run(["ipatgo.exe", "data", inetid, user_number, passowrd, pars_number, vdata])
```

<div id='id-section10'/>

## Windows/macOSX/Linuxでコマンドプロントからの【dataモード】

### Windows

```
C:\umagen\ipatgo>ipatgo.exe data XXXXXXXX 99999999 9999 9999 20190526,TOKYO,11,FUKUSYO,NORMAL,,06,100
```

### macOSX
* ipatgoファイルに実行権限を付与する必要があります

```
$ ipatgo data XXXXXXXX 99999999 9999 9999 20190526,TOKYO,11,FUKUSYO,NORMAL,,06,100 --nosplash
```

### Linux
* ipatgoファイルに実行権限を付与する必要があります

```
$ ipatgo data XXXXXXXX 99999999 9999 9999 20190526,TOKYO,11,FUKUSYO,NORMAL,,06,100 --nosplash
```
