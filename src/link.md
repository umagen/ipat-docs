# リンク

## IPATGOを連携利用しているソフトウェア

* <a href="https://ipatgox.blog.fc2.com/" target="blank">IPATGOX</a>
    * 速報オッズをもとに全馬を分析し、過剰投票されている馬をリアルタイムで把握できるシステムです。さらに、そのまま即時投票することも可能です。
<br/><br/>
* <a href="https://balloonx.blog.fc2.com/" target="blank">BALLOONX</a>
    * 投票の締め切り前に通知バルーンでお知らせする機能や、タスクバーから直接、即パット入出金を行えるなど、多機能な便利ツールです。
<br/><br/>
* <a href="http://www.mmslsoft.jp/index.html" target="blank">TripleAutoBetSX / SuperWIN5</a>
    * 予想投票馬券選出、PAT自動投票、的中判定、口座収支管理、全自動で終日運転
<br/><br/>
* <a href="http://bigdream.my.coocan.jp/" target="blank">BigdreamExplosion</a>
    * 作者一生の集大成、無人自動投票、Bigdream予想に特化した予想で馬券長者出現の可能性有ります。
<br/><br/>
* <a href="http://www.layered.co.jp/" target="blank">市丸博司の「馬券エージェント」</a>
    * 市丸博司の御用達、ＩＰＡＴ投票なら馬券エージェントにおまかせあれ
<br/><br/>

## 関連リンク

* <a href="https://jra-van.jp/dlb/sft/lib/ipatvotetool.html" target="blank">JRA-VANデータラボ</a>
    * JRA公式データを利用した競馬ソフトが使い放題！BALLOOXやIPATGOX（IPATGO）は、当サイトの会員専用ソフトとしてご利用いただけます。
<br/><br/>
* <a href="https://store.line.me/stickershop/product/1024712/ja" target="blank">LINEスタンプ ふくろうたちの午後</a>
    * ふくろうたちが日常を彩ります。メンフクロウ、白フクロウ、アフリカコノハズクがだれています