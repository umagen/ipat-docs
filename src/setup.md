# セットアップ

ダウンロードした`IPATGO_Vxxx_setup.exe`を実行してください

![def1](_static/images/smartsc1.png)
<br/>
このウィンドウが表示された場合は【詳細情報】をクリック
<br/><br/><br/>
![def2](_static/images/smartsc2.png)
<br/>
【実行】をクリック
<br/><br/><br/>
![UAC](_static/images/uac300.png)
<br/>
このウィンドウが表示された場合は【はい】をクリック
<br/><br/><br/>

![lha](_static/images/setup01.png)
<br/>
【上記の利用規約に同意します】にチェックを入れ 【次へ】をクリック。
<br/><br/><br/>
![upd](_static/images/setup02.png)
<br/>
【次へ】をクリックすると標準の`C:\umagen\ipatgo`にファイルがコピーされます。
違う場所にコピーしたい場合は、【参照】からコピー先のフォルダーを変更します。
<br/><br/><br/>
![upd](_static/images/setup03.png)
<br/>
【完了】をクリックします。
<br/><br/><br/>
![lha](_static/images/s2.png)
<br/>
上記のようにコピーされ終了です。<br/><br/>
## ソフトウェア ファイル構成
```
C:\umagen
    └─ipatgo
        │  ipatgo.exe-------------メインソフトウェア
        │  font.exe---------------サブプログラム
        │  demo.exe---------------デモソフトウェア
        │  readme.txt-------------ドキュメントファイル
        │
        ※以下はソフト実行時に必要に応じ作成されます
        │
        │　receipt.csv------------historyモード投票履歴一覧ファイル
        │　result.csv-------------historyモード投票履歴結果内容ファイル
        │　stat.ini---------------statモード照会ファイル
        │　demo.ini---------------デモソフトウェア設定保存ファイル
        │　yyyyMMdd.log-----------エラーログファイル
        └─
```
## プログラム削除方法
標準のままのコピー先の場合は`C:\umagen\ipatgo`をフォルダ毎削除するだけです。レジストリの変更はしていません。
