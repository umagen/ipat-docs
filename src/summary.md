# IPATGOについて


## 対応馬券式
単勝 / 複勝 / 枠連 / 馬連 / ワイド / 馬単 / 三連複 / 三連単 / WIN5

## 対応投票方式
通常 / フォーメーション / ボックス / ながし / 軸1頭ながし / 軸2頭ながし / 1着ながし※ / 2着ながし※ / 3着ながし※ / 1-2着ながし※ / 1-3着ながし※ / 2-3着ながし※

※マルチ投票指定可

## 購入状況照会
照会日付 / 照会時刻 / 累計購入金額 / 累計払戻金額 / 1日分購入金額 / 1日分払戻金額 / 購入限度額 / 購入可能件数

## 投票履歴照会
最終受付分 / 当日分 / 前日分

## 即パット入出金
即パット入金指示 / 即パット出金指示

## 動作条件
* <a href="https://jra-van.jp/dlb/" target="blank">JRA-VANデータラボ会員</a>でJV-Linkがインストールされている事（会員で無い場合はメールでお問い合わせください）
* <a href="http://www.jra.go.jp/dento/welcome/soku/so_kanyu.html" target="blank">JRA-IPAT会員</a>である事
* 対応OS Windows7 Windows8 Windows8.1 Windows10 Windows11
* Linux、Mac OS Xにも対応。詳細はお問い合わせください。

## 推奨環境
* CPU Celeron 2.8GHｚ以上
* メモリ 2GB以上
* HDD 20MB以上
* 解像度 1024×768以上
* 回線速度 FTTH(光)回線以上

## 利用料金
<a href="https://jra-van.jp/dlb/" target="blank">JRA-VANデータラボ会員</a>は無料

## E-Mail（お問い合わせ先）
<a href="mailto:umagen.pat@gmail.com">umagen.pat@gmail.com</a>

## Twitter
<a href="https://twitter.com/umagen_ipatgo" target="blank">![twitterlogo](_static/images/Twitter.png)</a> フォローにて重要なお知らせや更新情報を受け取れます（基本的にお知らせがある時にしかツイートしません）

