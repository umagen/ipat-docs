# ダウンロード

## IPATGOのダウンロード

|ファイル|対応OS|バージョン|備考|
|:--|:--|:--|:--|
|<a href="http://www.umagen.sakura.ne.jp/ipatgox/Soft/IPATGOX_Ver415_Setup.zip">IPATGOX_Ver415_Setup.zip</a>|Windows|Ver4.15 (2023/7/9)|■JRA-VAN DataLab会員は無料<br>■セットアップによりIPATGOのVer4.15が<br>　c:\umagen\ipatgo にインストールされます|
|<a href="http://www.umagen.sakura.ne.jp/ipatgo/macos/IPATGO_V420_macOS.zip">IPATGO_V420_macOS.zip</a>|MacOSX|Ver4.20 (2023/7/20)||
|<a href="http://www.umagen.sakura.ne.jp/ipatgo/linux/IPATGO_V415_Linux.zip">IPATGO_V415_Linux.zip</a>|Linux|Ver4.15 (2023/7/9) ||

<br>

※ ダウンロードが始まらない場合は、リンクを右クリックして「名前を付けてリンク先を保存」<br>
※ ダウンロードがうまくいかない場合は<a href="https://ipatgox.blog.fc2.com/blog-entry-2.html" target="blank">こちら</a>の解説を参考にしてください <br>
※ MacOSX版 / Linux版はipatgoファイルに実行権限を付与する必要があります<br>
<br>

## twitter

※ <a href="https://twitter.com/umagen_ipatgo" target="blank">フォロー</a>にてIPATGO/BALLOONXの重要なお知らせや更新情報が受け取れます
 基本的にお知らせがある時にしかツイートしません 

