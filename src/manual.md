# モード（機能）一覧

## 6つのモード(機能)があります

1. [【dataモード】 投票データ1件分を投票 ](#id-section1)
1. [【fileモード】 投票ファイルから複数件一括投票 （全馬券式 / WIN5）](#id-section2)
1. [【statモード】 購入状況照会 ](#id-section3)
1. [【historyモード】 投票履歴照会 （当日分 / 前日分 / 最終受付分）](#id-section4)
1. [【depositモード】 即パット入金指示 ](#id-section5)
1. [【withdrawモード】 即パット出金指示 ](#id-section6)

 [（補足）コマンドプロンプトより実行ファイルの情報を得る方法](#id-section7)

<br/>

<div id='id-section1'/>

## dataモード（投票データ1件分を投票）※全馬券式対応

|書式|ipatgo.exe data INET-ID 加入者番号 暗証番号 P-ARS番号 投票データ|
|:--|:--|
|コマンド例|ipatgo.exe data abcdefgh 12345678 1111 9999 20190706,TOKYO,9,TANSYO,NORMAL,,03,100|
||各項目をコードで表記、半角スペースで区切りパラメータを渡します|
|戻り値|0 : 投票処理は正常に行われた|
||0以外 : 投票処理に異常があった（エラー内容はログファイルで確認できます）|
|備考|* 進捗ウィンドウを非表示にして連続実行する場合は実行毎にウェイトを1秒いれてください|

<br/>
<div id='id-section2'/>

## fileモード（投票データファイルから複数件一括投票）※WIN5を除く全馬券式

|書式|ipatgo.exe file INET-ID 加入者番号 暗証番号 P-AR番号 投票ファイルパス|
|:--|:--|
|コマンド例|ipatgo.exe file abcdefgh 12345678 1111 9999 c:\UMAGEN\IPATGO\votefile.csv|
||各項目をコードで表記、半角スペースで区切りパラメータを渡します|
|戻り値|0 : 投票処理は正常に行われた|
||0以外 : 投票処理に異常があった（エラー内容はログファイルで確認できます）|
|備考|* 投票ファイルパスに半角スペースが含まれている場合はダブルクォートで囲みます|
||* ファイルにWIN5投票データを含めてはいけません|
||* 進捗ウィンドウを非表示にして連続実行する場合は実行毎にウェイトを1秒いれてください|

<br/>

## fileモード（投票データファイルから複数件一括投票）※WIN5専用

|書式|ipatgo.exe file INET-ID 加入者番号 暗証番号 P-ARS番号 投票ファイルパス --win5|
|:--|:--|
|コマンド例|ipatgo.exe file abcdefgh 12345678 1111 9999 c:\UMAGEN\IPATGO\votefile.csv --win5|
||各項目をコードで表記、半角スペースで区切りパラメータを渡します|
|戻り値|0 : 投票処理は正常に行われた|
||0以外 : 投票処理に異常があった（エラー内容はログファイルで確認できます）|
|備考|* 投票ファイルパスに半角スペースが含まれている場合はダブルクォートで囲みます|
||* ファイルにWIN5以外の投票データを含めてはいけません|
||* 進捗ウィンドウを非表示にして連続実行する場合は実行毎にウェイトを1秒いれてください|

<br/>
<div id='id-section3'/>

## statモード（購入状況照会）

|書式|ipatgo.exe stat INET-ID 加入者番号 暗証番号 P-ARS番号|
|:--|:--|
|コマンド例|ipatgo.exe stat abcdefgh 12345678 1111 9999|
||各項目をコードで表記、半角スペースで区切りパラメータを渡します|
|戻り値|0 : 購入状況を正常に照会できた|
||0以外 : 照会処理に異常があった（エラー内容はログファイルで確認できます）|
|照会値|`ipatgo.exe`と同じ場所に`stat.ini`が作成され照会値が書き込まれます|
|(stat.ini)セクション|[stat]|
|(stat.ini)キー|date=照会年月日|
||time=照会時刻|
||total_vote_amount=累計購入金額|
||total_repayment=累計払戻金額|
||daily_vote_amount=1日分購入金額|
||daily_repayment=1日分払戻金額|
||limit_vote_amount=購入限度額|
||limit_vote_count=購入可能件数|
||<a href="http://www.umagen.sakura.ne.jp/ipatgo/docs/data/stat/stat.ini" target="blank">stat.ini</a>　（サンプルデータ）|
|備考|* 進捗ウィンドウは表示されません|
||* IPAT公式サイトからの情報取得につき、IPATGOを介さないIPATサイトからの直接の購入を含め反映されます|

<br/>
<div id='id-section4'/>

## historyモード（投票履歴照会）※当日分

|書式|ipatgo.exe history INET-ID 加入者番号 暗証番号 P-ARS番号|
|:--|:--|
|コマンド例|ipatgo.exe history abcdefgh 12345678 1111 9999|
||各項目をコードで表記、半角スペースで区切りパラメータを渡します|
|戻り値|0 : 投票履歴を正常に照会できた|
||0以外 : 照会処理に異常があった（エラー内容はログファイルで確認できます）|
|照会値|`ipatgo.exe`と同じ場所に2つのファイル`receipt.csv` `result.csv`が作成され照会値が書き込まれます|
||2つのファイルのリンクキーは[受付番号]です|
|投票履歴一覧ファイル|照会内容,受付番号,受付時刻,件数,払戻額|
||<a href="http://www.umagen.sakura.ne.jp/ipatgo/docs/data/today/receipt.csv" target="blank">receipt.csv</a>　（サンプルデータ）|
|投票履歴結果内容ファイル|受付番号,件番号,レース,馬券投票方式,組番,組数,投票金額,的中判定|
||<a href="http://www.umagen.sakura.ne.jp/ipatgo/docs/data/today/result.csv" target="blank">result.csv</a>　（サンプルデータ）|
|的中判定のコード|1:確定前 5:的中 6:外れ 7:返還　※これ以外の未知のコードが戻り値となる場合があります|
|備考|* 進捗ウィンドウは表示されません|
||* 払戻額は受付番号毎の合計額でしか取得出来ません|
||* 照会値の馬券投票方式や組番の書式はIPATGOの投票データフォーマットとは異なります|
||* IPAT公式サイトからの情報取得につき、IPATGOを介さないIPATサイトからの直接の投票を含め反映されます|
||* 海外競馬への投票情報は反映されません|

<br/>
## historyモード（投票履歴照会）※前日分

|書式|ipatgo.exe history INET-ID 加入者番号 暗証番号 P-ARS番号 --before|
|:--|:--|
|コマンド例|ipatgo.exe history abcdefgh 12345678 1111 9999 --before|
||各項目をコードで表記、半角スペースで区切りパラメータを渡します|
|戻り値|0 : 投票履歴を正常に照会できた|
||0以外 : 照会処理に異常があった（エラー内容はログファイルで確認できます）|
|照会値|`ipatgo.exe`と同じ場所に2つのファイル`receipt.csv` `result.csv`が作成され照会値が書き込まれます|
||2つのファイルのリンクキーは[受付番号]です|
|投票履歴一覧ファイル|照会内容,受付番号,受付時刻,件数,払戻額|
||<a href="http://www.umagen.sakura.ne.jp/ipatgo/docs/data/before/receipt.csv" target="blank">receipt.csv</a>　（サンプルデータ）|
|投票履歴結果内容ファイル|受付番号,件番号,レース,馬券投票方式,組番,組数,投票金額,的中判定|
||<a href="http://www.umagen.sakura.ne.jp/ipatgo/docs/data/before/result.csv" target="blank">result.csv</a>　（サンプルデータ）|
|的中判定のコード|1:確定前 5:的中 6:外れ 7:返還　※これ以外の未知のコードが戻り値となる場合があります|
|備考|* 進捗ウィンドウは表示されません|
||* 払戻額は受付番号毎の合計額でしか取得出来ません|
||* 照会値の馬券投票方式や組番の書式はIPATGOの投票データフォーマットとは異なります|
||* IPAT公式サイトからの情報取得につき、IPATGOを介さないIPATサイトからの直接の投票を含め反映されます|
||* 海外競馬への投票情報は反映されません|

<br/>
## historyモード（投票履歴照会）※最終受付分

|書式|ipatgo.exe history INET-ID 加入者番号 暗証番号 P-ARS番号 --latest|
|:--|:--|
|コマンド例|ipatgo.exe history abcdefgh 12345678 1111 9999 --latest|
||各項目をコードで表記、半角スペースで区切りパラメータを渡します|
|戻り値|0 : 投票履歴を正常に照会できた|
||0以外 : 照会処理に異常があった（エラー内容はログファイルで確認できます）|
|照会値|`ipatgo.exe`と同じ場所に2つのファイル`receipt.csv` `result.csv`が作成され照会値が書き込まれます|
||2つのファイルのリンクキーは[受付番号]です|
|投票履歴一覧ファイル|照会内容,受付番号,受付時刻,件数,払戻額|
||<a href="http://www.umagen.sakura.ne.jp/ipatgo/docs/data/latest/receipt.csv" target="blank">receipt.csv</a>　（サンプルデータ）|
|投票履歴結果内容ファイル|受付番号,件番号,レース,馬券投票方式,組番,組数,投票金額,的中判定|
||<a href="http://www.umagen.sakura.ne.jp/ipatgo/docs/data/latest/result.csv" target="blank">result.csv</a>　（サンプルデータ）|
|的中判定のコード|1:確定前 5:的中 6:外れ 7:返還　※これ以外の未知のコードが戻り値となる場合があります|
|備考|* 進捗ウィンドウは表示されません|
||* 払戻額は受付番号毎の合計額でしか取得出来ません|
||* 照会値の馬券投票方式や組番の書式はIPATGOの投票データフォーマットとは異なります|
||* IPAT公式サイトからの情報取得につき、IPATGOを介さないIPATサイトからの直接の投票を含め反映されます|
||* 海外競馬への投票情報は反映されません|

<br/>

<div id='id-section5'/>

## depositモード（即パット入金指示）

|書式|ipatgo.exe deposit INET-ID 加入者番号 暗証番号 P-ARS番号 入金金額|
|:--|:--|
|コマンド例|ipatgo.exe deposit abcdefgh 12345678 1111 9999 30000|
||各項目をコードで表記、半角スペースで区切りパラメータを渡します|
|戻り値|0 : 入金処理は正常に行われた|
||0以外 : 入金処理に異常があった（エラー内容はログファイルで確認できます）|
|備考|* 進捗ウィンドウは表示されません|
||* 入金金額は100円単位の円指定です。1日あたり3回目からは手数料がかかります|

<br/>

<div id='id-section6'/>

## withdrawモード（即パット出金指示）

|書式|ipatgo.exe withdraw INET-ID 加入者番号 暗証番号 P-ARS番号|
|:--|:--|
|コマンド例|ipatgo.exe withdraw abcdefgh 12345678 1111 9999|
||各項目をコードで表記、半角スペースで区切りパラメータを渡します|
|戻り値|0 : 出金処理は正常に行われた|
||0以外 : 出金処理に異常があった（エラー内容はログファイルで確認できます）|
|備考|* 進捗ウィンドウは表示されません|
||* 出金金額は指定出来ません。必ず全額出金となります|

<br/>

<div id='id-section7'/>

## （補足）コマンドプロンプトより実行ファイルの情報を得る方法

ipatgo.exeのバージョン
```
C:\umagen\ipatgo>ipatgo.exe version
ipatgo version x.xx
```

ipatgo.exeのヘルプ
```
C:\umagen\ipatgo>ipatgo.exe help
Usage: ipatgo [--version] [--help] <command> [<args>]

Available commands are:
    data
    deposit
    file
    history
    stat        Get purchase Stat
    version     Print ipatgo version and quit
    withdraw
```